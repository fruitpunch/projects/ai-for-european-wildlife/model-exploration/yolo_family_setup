from fruitpunchwildlife import datasets
from pprint import pprint 
import argparse
import sys


# Install the library executing in collab
# !pip install git+https://github.com/maguelo/fruitpunchwildlife


  #"/home/pilarnavarro/pilar/fruitpunch/AI for european wildlife/data" #"dataset"  # Path to the dataset

if __name__=='__main__':

    argparser = argparse.ArgumentParser()
    argparser.add_argument("--dataset-path", type=str, required=True, help="Path to folder with the data folders.")
    argparser.add_argument("--seed", type=int, default=42, help="Seed for the random operations. Default: 42.")
    argparser.add_argument("--balancing-fields", nargs='+', default=['commonName','deploymentID_category'], help="Fields to balance the dataset. Default: ['commonName','deploymentID_category'].")
    argparser.add_argument("--stratify-fields", nargs='+', default=None, help="Fields to stratify the dataset when splitting in train and test. Default: None.")
    argparser.add_argument("--num-classes", type=int, default=None, help="Number of most common classes to keep. None value show the current index. Default: None.")
    argparser.add_argument("--max-samples", type=int, default=1000, help="Max number of samples per class to keep. Default: 1000.")
    argparser.add_argument("--test-size", type=float, default=0.2, help="Percentage of samples to keep in the test set. Default: 0.2.")
    argparser.add_argument("--prepare-nas", action='store_true', help="Prepare the dataset and folders structure for training of YOLO-NAS. Default: False." )

    arguments = argparser.parse_args()

    DATASET_PATH = arguments.dataset_path     

    DATASET_DICT={
                "trapper_photos_13":{"metadata": f"{DATASET_PATH}/trapper_photos_13/metadata.csv", "path": f"{DATASET_PATH}/trapper_photos_13"},
                "trapper_photos_2":{"metadata": f"{DATASET_PATH}/trapper_photos_2/metadata.csv", "path": f"{DATASET_PATH}/trapper_photos_2"},
                "trapper_photos_6":{"metadata": f"{DATASET_PATH}/trapper_photos_6/metadata.csv", "path": f"{DATASET_PATH}/trapper_photos_6"},
                # "RE":{"metadata": f"{DATASET_PATH}/RE/metadata.csv", "path": f"{DATASET_PATH}/RE"},
            }

    data_loader = datasets.DatasetLoader(DATASET_PATH, DATASET_DICT, seed=arguments.seed)
    data_loader.prepare_datasets()
    data_loader.generate_index()
    data_loader.apply_new_index()

    if arguments.num_classes is None:
      print(data_loader.new_index.to_markdown(index=False))
      print ("Please provide the parameter --num-classes with the class ")
      print("Please select the class id you want to use as a threshold. This class will be included. \nExample: If you select 18. The model would take from class_id 0 - 18. 18 included")
      sys.exit(1)
    if arguments.num_classes == 0: 
      arguments.num_classes= None

    data_loader.create_annotations()

    MAIN_FIELDS =arguments.balancing_fields

    balanced_df=data_loader.create_balanced_dataset(main_fields=MAIN_FIELDS, class_id_filter=arguments.num_classes, max_samples=arguments.max_samples)



    list_region_name = [(val[0],val[1], val[2], cnt) for val, cnt in balanced_df[MAIN_FIELDS+['class_id_updated']].value_counts().items()]
                # list_values = {val for val, cnt in df_full.deploymentID_region.value_counts().items()}
    list_region_name=sorted(list_region_name, key=lambda x: x[0])
    # pprint(list_region_name)


    split_dict=datasets.split_balanced_dataset(balanced_df, stratify_fields=arguments.stratify_fields, test_size=arguments.test_size, seed=arguments.seed)        

    # Generate dataset.yaml
    print(datasets.prepare_yolov58(split_dict,balanced_df))

    #Work in progress already generated the folders with the images and labels with the new category id
    if arguments.prepare_nas:
      datasets.prepare_yolonas(split_dict, 'nas_dataset')


