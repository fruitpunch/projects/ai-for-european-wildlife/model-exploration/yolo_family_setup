# YOLO family setup

Data Preparation Repository for the YOLO Family of Models. 

This library performs the following:
- Load datasets, clean, merge, and balance them.
- Split dataset into train, validation, and test sets.
- Create yolov5 and yolov8 dataset.yaml file
- Create directory structure for YOLONAS

## Usage

In order for this library to work properly, you should create a directory contaning a folder for each dataset which, in turn, should contain the images for that dataset and a `metadata.csv` file with the information of the images. For example, if you have the `trapper_photos_6` and `trapper_photos_13` datasets, they should be included inside another directory, called, e.g., `data`. 

To run the library, you should run the `main.py` file with the following required argument: 

- `--dataset-path`: Path to the parent directory containing the datasets. In the example above, it would be `data`.

The `main.py` script can also take the following optional arguments:

- `--seed`: Seed for the random operations. The default value is 42.
- `--balancing-fields`: Fields to balance the dataset. By default, common name and deploymentID are used. 
- `--stratify-fields`: Fields to stratify the dataset when splitting in train and test. By default, no stratification is performed.
- `--num-classes`: Number of most common classes to keep. The rest of the classes will be deleted. The default value is 18.
- `--max-samples`: Max number of samples per class to keep. The default value is 1000.
- `--test-size`: Percentage of samples to use for the test/validation sets. The default value is 20%.
- `--prepare-nas`: Indicates whether to prepare the dataset and folders structure for training of YOLO-NAS. By default it is set to False.
- `--help`: Shows the help message.

Example of usage:

```bash
python main.py --dataset-path ./data --seed 42 --balancing-fields commonName deploymentID_category --stratify-fields commonName deploymentID_category --num-classes 18 --max-samples 1000 --test-size 0.2 --prepare-nas
```

**Author**: Miguel Maldonado


## Clarification of the params

### num-classes

This parameter works as a threshold to generate a subset of the dataset if you set it to 10. Only would take classes 0 - 10 from the following example.

Values:
* Number -> Threshold. works normally
* None -> Show the current index table and stop the execution *DEFAULT*
* 0 -> Ignore the threshold and use all class

|   class_id | commonName             |   count |
|-----------:|:-----------------------|--------:|
|          0 | Reindeer               |    3005 |
|          1 | Domestic Cat           |    2370 |
|          2 | Roe Deer               |    1945 |
|          3 | Eurasian Magpie        |    1374 |
|          4 | Mountain Hare          |    1135 |
|          5 | Red Fox                |     890 |
|          6 | Eurasian Elk           |     807 |
|          7 | Domestic Dog           |     517 |
|          8 | Red Deer               |     495 |
|          9 | Eurasian Red Squirrel  |     327 |
|         10 | Chicken                |     196 |
|         11 | Hooded Crow            |     120 |
|         12 | Eurasian Tree Sparrow  |     114 |
|         13 | European Robin         |     113 |
|         14 | Eurasian Lynx          |     107 |
|         15 | Fallow Deer            |     103 |
|         16 | Spotted Nutcracker     |      78 |
|         17 | Fieldfare              |      76 |
|         18 | Song Thrush            |      71 |
|         19 | European Pine Marten   |      63 |
|         20 | Hazel Grouse           |      61 |
|         21 | West European Hedgehog |      45 |
|         22 | Great Tit              |      33 |
|         23 | Common Gull            |      31 |
|         24 | Common Crane           |      29 |
|         25 | European Badger        |      28 |


### balancing-fields and max-samples

Fields used to balance the dataset using the provided fields (Default: ['commonName','deploymentID_category']). 
For example, for Reindeer, the dataset contains 3005 samples in 5 deploymentID_category (deployment areas). 
With the following configuration 

--balancing-fields ['commonName','deploymentID_category']

--max-samples 100

it collects 100 samples per deploymentID_category, so the max number of samples for each commonName would be 500.


### stratify-fields

if stratify-fields is enabled when the final split train, test, val is generated would try to keep samples from all columns provided as parameter represented in all sets

Example: ['commonName','deploymentID_category']